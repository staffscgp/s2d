#include "stdafx.h"
#include "SoundEffect.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace S2D
{
	SoundEffect::SoundEffect(void)
	{
		_isLoaded = AL_FALSE;
		_isLooping = AL_FALSE;
		_pitch = 1.0f;
		_gain = 1.0f;
	}

	SoundEffect::SoundEffect(bool looping, float pitch, float gain)
	{
		_isLoaded = AL_FALSE;
		_isLooping = looping;
		_pitch = pitch;
		_gain = gain;
	}

	SoundEffect::~SoundEffect(void)
	{
		alDeleteSources(1, &_source);
		alDeleteBuffers(1, &_bufferID);
	}

	ALuint SoundEffect::GetSource()
	{
		return _source;
	}

	SoundEffectState SoundEffect::GetState()
	{
		ALint state;
		alGetSourcei(_source, AL_SOURCE_STATE, &state);

		switch (state)
		{
		case AL_PLAYING:
			return SoundEffectState::PLAYING;
		case AL_STOPPED: case AL_INITIAL:
			return SoundEffectState::STOPPED;
		case AL_PAUSED:
			return SoundEffectState::PAUSED;
		default:
			return SoundEffectState::UNKNOWN;
		}
	}

	float SoundEffect::GetPitch()
	{
		return _pitch;
	}

	float SoundEffect::GetGain()
	{
		return _gain;
	}

	bool SoundEffect::IsLoaded()
	{
		return _isLoaded;
	}

	bool SoundEffect::IsLooping()
	{
		return _isLooping;
	}

	bool SoundEffect::Load(const char* soundFileName)
	{
		alGenBuffers(1, &_bufferID);
		alGenSources(1, &_source);
		if(alGetError() != AL_NO_ERROR) return false;

		_bufferID = alureCreateBufferFromFile(soundFileName);

		alSourcei(_source, AL_BUFFER, _bufferID);            
		alSourcef(_source, AL_PITCH, _pitch); 
		alSourcef(_source, AL_GAIN, _gain);
		alSourcei(_source, AL_LOOPING, _isLooping );

		_isLoaded = AL_TRUE;
		return true;
	}

	void SoundEffect::SetLooping(bool looping)
	{
		_isLooping = looping;
		alSourcei(_source, AL_LOOPING, _isLooping);
	}

	void SoundEffect::SetPitch(float pitch)
	{
		_pitch = pitch;
		alSourcef(_source, AL_PITCH, pitch);
	}

	void SoundEffect::SetGain(float gain)
	{
		_gain = gain;
		alSourcef(_source, AL_GAIN, gain);
	}

	/* When setting Offset on a SoundEffect which is already playing, the playback will
	   jump to the new offset. If the source is not playing, then the offset will be
	   applied on the next Audio::Play call. */
	void SoundEffect::Offset(float offset)
	{
		alSourcef(_source, AL_SEC_OFFSET, offset);
	}
}
