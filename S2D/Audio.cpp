#include "stdafx.h"
#include "Audio.h"

namespace S2D
{
	namespace Audio
	{
		namespace
		{
			bool _isInitialised;
			ALCdevice *_device;                                                          //Create an OpenAL Device
			ALCcontext *_context;                                                        //And an OpenAL Context
		}

		bool Audio::IsInitialised()
		{
			return _isInitialised;
		}

		void Audio::Initialise()
		{
			_isInitialised = true;

			_device = alcOpenDevice(NULL);                                               //Open the device
			if(_device == nullptr)
			{
				_isInitialised = false;													//Error during device oening
				return;
			}

			_context = alcCreateContext(_device, NULL);                                   //Give the device a context
			alcMakeContextCurrent(_context);                                             //Make the context the current
			if(_context == nullptr)
			{
				_isInitialised = false;
				return;
			}
		}

		void Audio::Destroy()
		{
			if (_isInitialised)
			{
				alcMakeContextCurrent(NULL);                                                //Make no context current
				alcDestroyContext(_context);                                                 //Destroy the OpenAL Context
				alcCloseDevice(_device);                                                     //Close the OpenAL Device 
			}
		}

		bool Audio::Play(SoundEffect* soundEffect)
		{
			if (soundEffect->IsLoaded())
				alSourcePlay(soundEffect->GetSource());                                          //Play the sound buffer linked to the source
			else
				return false;

			if(alGetError() != AL_NO_ERROR) 
				return false; //Error when playing sound

			return true;
		}

		bool Audio::Stop(SoundEffect* soundEffect)
		{
			if (soundEffect->IsLoaded())
				alSourceStop(soundEffect->GetSource());                                          //Play the sound buffer linked to the source
			else
				return false;

			if(alGetError() != AL_NO_ERROR) 
				return false; //Error when playing sound

			return true;
		}

		bool Audio::Pause(SoundEffect* soundEffect)
		{
			if (soundEffect->IsLoaded())
				alSourcePause(soundEffect->GetSource());                                          //Play the sound buffer linked to the source
			else
				return false;

			if(alGetError() != AL_NO_ERROR) 
				return false; //Error when playing sound

			return true;
		}

		bool Audio::Resume(SoundEffect* soundEffect)
		{
			if (soundEffect->IsLoaded())
				alSourcePlay(soundEffect->GetSource());                                          //Play the sound buffer linked to the source
			else
				return false;

			if(alGetError() != AL_NO_ERROR) 
				return false; //Error when playing sound

			return true;
		}
	}
}
