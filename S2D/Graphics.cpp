#include "stdafx.h"
#include "Graphics.h"

#include <sstream>

namespace S2D
{
	namespace Graphics
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Declaration of Private Properties in an Anonymous Namespace										   //
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		namespace
		{
			Game* game;
			int elapsedTime = 0;
			int previousUpdateTime = 0;
			int viewportWidth = 800;
			int viewportHeight = 600;
			bool isFixedTimeStep = false;
			int preferredFPS = 60;
			bool isFullScreen = false;
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Forward declared methods in Graphics CPP to keep private											   //
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		/// <summary>	Static Method for the GLUT Display Callback - Used to Draw Game. </summary>
		void Display();

		/// <summary>
		/// Static Method for the GLUT Display Callback - Used to alter the viewport when the window is
		/// resized.
		/// </summary>
		///
		/// <param name="width">	The width. </param>
		/// <param name="height">	The height. </param>
		void Reshape(int width, int height);

		/// <summary>
		/// Static Method for the GLUT Display Callback - Used to Update and Refresh game with
		/// FixedTimeStep.
		/// </summary>
		///
		/// <param name="value">	The ms value. </param>
		void Tick(int value);

		/// <summary>
		/// Static Method for the GLUT Display Callback - Used to Update and Refresh game with no
		/// FixedTimeStep.
		/// </summary>
		void Idle();

		/// <summary>
		/// Static Method for the GLUT Display Callback - Used to Update Game and calculates elapsed time
		/// since last Update.
		/// </summary>
		void Update();

		/// <summary>
		/// Static Method for the GLUT Display Callback - Used to Draw Game and calculates elapsed time
		/// since last Draw.
		/// </summary>
		void Draw();

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// The Graphics Manager Namespace Methods																	   //
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		int Graphics::GetViewportWidth() { return Graphics::viewportWidth; }
		int Graphics::GetViewportHeight() { return Graphics::viewportHeight; }
		bool Graphics::IsFixedTimeStep() { return Graphics::isFixedTimeStep; }
		int Graphics::GetPreferredFPS() { return Graphics::preferredFPS; }

		void Graphics::Initialise(int argc, char* argv[], Game* game, int viewportWidth, int viewportHeight, bool fullScreen, int windowX, int windowY)
		{
			Graphics::Initialise(argc, argv, game, viewportWidth, viewportHeight, fullScreen, windowX, windowY, "S2D Engine");
		}

		void Graphics::Initialise(int argc, char* argv[], Game* game, int viewportWidth, int viewportHeight, bool fullScreen, int windowX, int windowY, int preferredFPS)
		{
			Graphics::Initialise(argc, argv, game, viewportWidth, viewportHeight, fullScreen, windowX, windowY, "S2D Engine", preferredFPS);
		}

		void Graphics::Initialise(int argc, char* argv[], Game* game, int viewportWidth, int viewportHeight, bool fullScreen, int windowX, int windowY, char* windowTitle, int preferredFPS)
		{
			Graphics::isFixedTimeStep = true; //Enables Fixed Time Step as a preferred FPS has been passed through
			Graphics::preferredFPS = preferredFPS; //Sets the preferred FPS

			Graphics::Initialise(argc, argv, game, viewportWidth, viewportHeight, fullScreen, windowX, windowY, windowTitle);
		}

		void Graphics::Initialise(int argc, char* argv[], Game* game, int viewportWidth, int viewportHeight, bool fullScreen, int windowX, int windowY, char* windowTitle)
		{
			if (game != nullptr) //Ensure we have passed a game object in
			{
				Graphics::game = game; //Store a pointer to the Game abstract class
				Graphics::viewportWidth = viewportWidth; //Stores Viewport data
				Graphics::viewportHeight = viewportHeight; //Stores Viewport data

				glutInit(&argc, argv); //Initialise GLUT
				glutInitDisplayMode(GLUT_DOUBLE);//Double Buffer App

				if (Graphics::isFixedTimeStep) //Attempts to refresh at a fixed time step
				{
					if (Graphics::preferredFPS == 0) Graphics::preferredFPS = 60; //Can't have 0 FPS - It makes no sense!
					int refreshMilliseconds = (int)(1000.0f / (float) Graphics::preferredFPS); //Casts included for clarity
					glutTimerFunc(refreshMilliseconds, Graphics::Tick, refreshMilliseconds); //Calls a Timer function at a specific refresh rate
				}
				else //Refreshes as fast as possible - monitor refresh rate
				{
					glutIdleFunc(Graphics::Idle); //Called after the display method finishes
					
				}

				std::stringstream gameMode;
				gameMode << viewportWidth << "x" << viewportHeight;
				glutGameModeString(gameMode.str().c_str());
				if (fullScreen && glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
				{
					glutEnterGameMode();
				}
				else
				{
					glutInitWindowSize(viewportWidth, viewportHeight); // Set the desired Viewport Size
					glutInitWindowPosition(windowX, windowY);
					glutCreateWindow(windowTitle); // Create the Window with the Window Title
				}

				glutDisplayFunc(Graphics::Display); //Calls the update and draw methods
				glutReshapeFunc(Graphics::Reshape); //Called when Window is resized

				glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

				glEnable(GL_TEXTURE_2D);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glEnable(GL_BLEND);

				game->LoadContent(); //Load the content of the game
			}
		}

		void Graphics::Destroy()
		{
			glutLeaveMainLoop();
		}

		void Graphics::StartGameLoop()
		{
			glutMainLoop(); //Start the game loop - This begins the Display/Idle/Timer functions
		}

		void Graphics::SetClearColor(const Color* color)
		{
			glClearColor(color->R, color->G, color->B, 0.0f);
		}

		void Graphics::SetClearColor(float r, float g, float b)
		{
			glClearColor(r, g, b, 0.0f);
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// The Private Graphics Manager Namespace Methods													   //
		/////////////////////////////////////////////////////////////////////////////////////////////////////////

		void Display()
		{
			Graphics::Draw(); //Redraws the game
		}

		void Reshape(int width, int height)
		{
			Graphics::viewportWidth = width;
			Graphics::viewportHeight = height;

			glViewport(0, 0, (GLsizei)width, (GLsizei)height); //Set up Viewport for 2D Rendering

			glMatrixMode(GL_PROJECTION); //Change to projection matrix
			glLoadIdentity();
			gluOrtho2D(0, width, height, 0); //Set up Ortho Projection

			glMatrixMode(GL_MODELVIEW); //Back to model view for drawing
		}

		//Idle function calls as fast as possible
		void Idle()
		{
			Graphics::Update(); //Updates the Game
			glutPostRedisplay(); //Causes the Display Function to be called again
		}

		//Timer function calls at a specific refresh rate
		void Tick(int value)
		{
			int updateTime = glutGet(GLUT_ELAPSED_TIME);
			Graphics::Update(); //Updates the Game
			updateTime = glutGet(GLUT_ELAPSED_TIME) - updateTime;
			glutPostRedisplay(); //Causes the Display Function to be called again
			glutTimerFunc(value - updateTime, Graphics::Tick, value); //Causes the Tick Function to be called again
		}

		void Update()
		{
			elapsedTime = glutGet(GLUT_ELAPSED_TIME); //Gets the time (ms) the application has been running - since glutInit

			if (previousUpdateTime == 0)
				game->Update(0);
			else
				game->Update(elapsedTime - previousUpdateTime); //Updates the game with the elapsed time since the last Update

			
		}

		void Draw()
		{
			if (previousUpdateTime == 0)
				game->Draw(0);
			else
				game->Draw(elapsedTime - previousUpdateTime); //Updates the game with the elapsed time since the last Draw

			previousUpdateTime = elapsedTime; //Stores the last elapsedTime
		}
	}
}